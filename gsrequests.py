import gamesparks
def authenticationRequest(password, userName, onResponse):
	request = {}
	request['password'] = password
	request['userName'] = userName
	gamesparks.sendWithData("AuthenticationRequest", request, onResponse)

def registrationRequest(displayName, password, userName, onResponse):
	request = {}
	request['displayName'] = displayName
	request['password'] = password
	request['userName'] = userName
	gamesparks.sendWithData("RegistrationRequest", request, onResponse)