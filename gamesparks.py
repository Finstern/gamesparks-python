#!/usr/bin/python
import websocket
import time
import json
import hmac
import hashlib
import base64
import logging
import collections

logging.basicConfig()

secretKey = None

socketUrl = None
authToken = None
sessionId = None

initialised = None
connected = None
error = None
closing = None

initCallback = None
messageCallback = None
errorCallback = None

pendingRequests = {}
webSocket = None

def initPreview(options):
    options['url'] = "wss://preview.gamesparks.net/ws/" + options['key']
    init(options)

def initLive(options):
    options['url'] = "wss://service.gamesparks.net/ws/" + options['key']
    init(options)

def init(options):
    global socketUrl
    socketUrl = options['url']
    global secretKey
    secretKey = options['secret']

    global initialised
    initialised = False;

    global connected
    connected = False;

    global error
    error = False;

    global closing
    closing = False;

    global initCallback
    initCallback = options['onInit']

    global messageCallback
    messageCallback = options['onMessage']

    global errorCallback
    errorCallback = options['onError']

    cleanup();
    connect();

def send(requestType, onResponse):
    sendWithData(requestType, {}, onResponse)

def sendWithData(requestType, requestJson, onResponse):
    if initialised == False:
        onResponse({error : "NOT_INITIALISED"})
    if requestType.startswith('.') == False:
        requestType = "."+requestType
    requestJson['@class'] = requestType
    requestJson['requestId'] = int(time.time())
    if onResponse != None:
        pendingRequests[requestJson['requestId']] = onResponse
        #TODO Set timeout

    requestString = json.dumps(requestJson)
    print "GsSend: " + requestString
    webSocket.send(requestString)


def cleanup():
    if webSocket != None:
        webSocket.websocket.on_close = None
        webSocket.close

def on_message(ws, message):
    global pendingRequests
    result = json.loads(message)
    print "GsRecv: " + message
    if 'authToken' in result.keys():
        global authToken
        authToken = result['authToken']
        del result['authToken']

    resultType = result['@class']

    if resultType == ".AuthenticatedConnectResponse":
        handshake(result)
    elif 'Response' in result['@class']:
        if 'requestId' in result.keys():
            requestId = result['requestId']
            del result['requestId']
            if requestId in pendingRequests.keys():
                print pendingRequests
                pendingRequests[requestId](result)
                pendingRequests[requestId] = None
    else:
        messageCallback(json.dumps(result))
    

def on_error(ws, error):
    errorCallback(error);
    error = True

def on_close(ws):
    connected = False
    if error == False:
        connect()

def on_open(ws):
    connected = True

def handshake(result):
    global socketUrl
    global sessionId
    global initialised
    if 'connectUrl' in result:
        socketUrl = result['connectUrl']
    elif 'nonce' in result:
        sig = base64.b64encode(hmac.new(secretKey,bytes(result['nonce']).encode('utf-8'), digestmod=hashlib.sha256).digest())
        toSend = {"@class":".AuthenticatedConnectRequest", "hmac" : sig}

        if authToken != None:
            toSend['authToken'] = authToken
        if sessionId != None:
            toSend['sessionId'] = sessionId

        webSocket.send(json.dumps(toSend))
    elif 'sessionId' in result:
        sessionId = result['sessionId']
        initialised = True
        initCallback()

def connect():
    websocket.enableTrace(False)
    global webSocket
    webSocket = websocket.WebSocketApp(socketUrl,
                                on_message = on_message,
                                on_error = on_error,
                                on_close = on_close)
    webSocket.on_open = on_open

    webSocket.run_forever()

def close_connection():
    webSocket.close()