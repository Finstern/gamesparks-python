#!/usr/bin/python

import json
import gamesparks
import gsrequests

#enter your apiKey and secretKey here
apiKey = ""
secretKey = ""

#Callbacks
def onInit():
	print "Initialised"
	#See gsRequests for this
	gsrequests.registrationRequest("Shaneo", "8472", "shaneo", loginResponse)

def onMessage(message):
	print "GsRecv: " + message

def onError(error):
	print "Error: " + error

def registerResponse(response):
	print response

def loginResponse(response):
	print json.loads(response)

def accountDetailsResponse(response):
	print json.loads(response)

gamesparks.initPreview({'key' : apiKey, 'secret' : secretKey, 'onInit' : onInit, 'onMessage' : onMessage, 'onError' : onError})
#gamesparks.initLive({'key' : apiKey, 'secret' : secretKey, 'onInit' : onInit, 'onMessage' : onMessage, 'onError' : onError})
